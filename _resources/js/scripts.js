
var s = window.screen;
var width = q.width = s.width;
var height = q.height = s.height;
var letters = Array(256).join(1).split('');

var draw = function () {
  q.getContext('2d').fillStyle='rgba(0,0,0,.05)';
  q.getContext('2d').fillRect(0,0,width,height);
  q.getContext('2d').fillStyle='#0F0';
  letters.map(function(y_pos, index){
    text = String.fromCharCode(3e4+Math.random()*33);
    x_pos = index * 10;
    q.getContext('2d').fillText(text, x_pos, y_pos);
    letters[index] = (y_pos > 758 + Math.random() * 1e4) ? 0 : y_pos + 10;
  });
};


$(document).ready( function () {
  bitcoinToUsd = $("#bitcoinToUsd");
  gbptousd = $("#gbptousd");
//bitcoin to usd
$.ajax({
    type: "GET",
    url: "https://api.coindesk.com/v1/bpi/currentprice/USD.json",
    dataType: "json",
    success: function(response){
        var bitcoinAmount = parseFloat(response.bpi.USD.rate_float);
        bitcoinToUsd.append(bitcoinAmount);
    }
});

//gbp to usd
$.ajax({
    type: "GET",
    url: "https://openexchangerates.org/api/latest.json?app_id=f684058c3b724f3283811a7c8cf30e3b",
    dataType: "json",
    success: function(response){
        var toUSD = response.rates.GBP;
        gbptousd.append(toUSD);
      // console.log(toUSD);
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
   console.log(XMLHttpRequest);
   console.log(textStatus);
   console.log(errorThrown);
}
});
});

$("#gbptobitcoin").click(function (event) {
  event.preventDefault();
  var bitcoinToUsdValue = bitcoinToUsd.text();
  var gbpToUsdValue = gbptousd.text();
  var gbpBtcExchangeRate = 1 / (parseFloat(bitcoinToUsdValue) * parseFloat(gbpToUsdValue));
  //console.log(gbpBtcExchangeRate);
  $("#showExchangeRate").append(gbpBtcExchangeRate);
  $("#showExchangeRate").fadeIn();
  $(".hide-on-click").addClass("hide-me");
  setInterval(draw, 40);
  return false;

})

function main() {
    "use strict";
    var counter = 0, all = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var $_inter = setInterval(function() {
        var text = document.getElementById("text");
        text.innerHTML = text.innerHTML.substring(0, counter) + all.charAt(Math.floor(Math.random()*all.length)) + text.innerHTML.substring(counter+1);
        counter = (counter+1)%text.innerHTML.length;
    }, 100);
}

var i = 0;
var txt = 'Lorem ipsum typing effect!'; /* The text */
var speed = 50; /* The speed/duration of the effect in milliseconds */

function typeWriter() {
  if (i < txt.length) {
    document.getElementById("text-above-canvas").innerHTML += txt.charAt(i);
    i++;
    setTimeout(typeWriter, speed);
  }
}
