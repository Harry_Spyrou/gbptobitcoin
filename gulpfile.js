/// <binding />
var gulp = require('gulp');
var concat = require('gulp-concat');
var autoprefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var htmlminify = require("gulp-html-minify");
// var imagemin = require('gulp-imagemin');
var jsmin = require('gulp-jsmin');
//var bourbon = require('bourbon').includePaths;
//var neat = require('bourbon-neat').includePaths;

var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

const settings = {
    srcFolder: '_resources/', //settings.srcFolder
    //newFormsSrcFolder: _'resources/'
    assetsFolder: 'build/assets/', //settings.assetsFolder
    layoutFolder: '_layouts/**/'  //settings.layoutFolder
};

gulp.task('compile-sass', function () {
    gulp.src([settings.srcFolder + 'scss/*', '!' + settings.srcFolder + 'scss/_**'])
        //.pipe(sass({
        //    includePaths: [bourbon, neat],
        //}).on('error', sass.logError))
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: [
                'last 5 version',
                'ie 9'
            ]
        }))
        .pipe(gulp.dest(settings.assetsFolder + 'css/'))

        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .pipe(rename({
            suffix: '.min'
        }))
    .pipe(gulp.dest(settings.assetsFolder + 'css/'))

    //.pipe(browserSync.stream());
})

gulp.task('build-html', function () {
    return gulp.src(settings.layoutFolder + "*.html")
        //.pipe(htmlminify())
        .pipe(gulp.dest("build/"))
});

gulp.task('minify-js', function () {
    gulp.src(settings.srcFolder + 'js/*.js')
        .pipe(gulp.dest(settings.assetsFolder + 'js/'))
        .pipe(concat('all.js'))
        .pipe(jsmin())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(settings.assetsFolder + 'js/'))
});

// gulp.task('imagemin', () =>
//     gulp.src(settings.srcFolder + 'img/*')
//         .pipe(imagemin())
//         .pipe(gulp.dest(settings.assetsFolder + 'img/'))
// );

gulp.task('browser-sync', function () {
   browserSync.init({
       server: {
           baseDir: './build',
           index: "index.html",
       }
   });
});
var bs = require("browser-sync").create();
gulp.task('default', ['build-html', 'compile-sass', 'minify-js', 'browser-sync'], function () {
    // place code for your default task here, don't forget to update every time you create/delete new functions'
    gulp.watch(settings.srcFolder + 'scss/**/*', ['compile-sass']);
    gulp.watch(settings.srcFolder + 'js/*', ['minify-js']);
    // gulp.watch(settings.srcFolder + 'img/*', ['imagemin']);
    gulp.watch(settings.layoutFolder + '*.html', ['build-html']);

});
